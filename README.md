# XR Hardware Rules

<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2020-2022, Collabora, Ltd. and the xr-hardware contributors
-->

A simple set of scripts to generate udev rules for user access to XR (VR and AR)
hardware devices.

Packages are developed over at <https://salsa.debian.org/rpavlik/xr-hardware>
rather than this repo, in an attempt to do the Debian stuff correctly by the
book.

## "Build" requirements (to re-generate files)

You'll need Python 3.7 or newer.

## Quick start: "build" and use

Once you have the deps installed, run:

```sh
make

sudo make install  # to install udev rules to /etc/udev/rules.d
```

`DESTDIR` is obeyed, but packagers should really see the dedicated section for
them below.

You'll need to at least re-plug your devices to get this to register for now.

Note that you still might see something like this in a simple `ls -l` view:

```none
crw-rw----+ 1 root root 246, 2 Jan 22 12:00 /dev/hidraw2
```

This doesn't mean it failed to work: the `+` means there's an ACL attached in
addition to the normal Unix permissions. Running `getfacl` on that device node
will reveal:

```none
# file: dev/hidraw2
# owner: root
# group: root
user::rw-
user:YOUR_USERNAME_HERE:rw-
group::---
mask::rw-
other::---
```

and you should be able to use the device without sudo. If this is not your
experience, file an issue.

## Makefile targets

Right now we're generating the hwdb files as well as the shorter associated
rules file, but not doing anything with it in the makefile.

If Python 3.x is not named `python3` you can override the `PYTHON` variable when
running `make` to tell it the correct name for your system.

### `all` or one of the generated files

This generates the file(s), if out of date.

### `test`

Runs some very minimal consistency checks on the database.

### `lint`

Runs `flake8` and `black` on the Python code so you'll need packages
`python3-flake8` and `black` or something like that.

### `clean`

Removes the generated files. Since these are committed to the repo, this will
"dirty" the repo.

### `install`

Probably run with `sudo`. Will install by default to `/etc/udev/rules.d`:
override `RULES_DIR` if you want them somewhere else. Obeys `DESTDIR` but not
`PREFIX`.

### For distro packages only

If you're a distro packager, use slightly different targets:

- Use `clean_package` instead of `clean`. (This is currently a no-op to avoid
  dirtying the repo.)
- Use `install_package` instead of `install` (installs to `/lib/udev/rules.d`,
  obeying both `DESTDIR` and `PREFIX`)

## Contributing

Contributions are assumed to be under the same license license as you received
with the files: the Inbound==Outbound convention.

Please submit a changelog fragment with your changes: see the `changes`
directory for details.

The device database is in `xrhardware/db.py` so that's the most likely file
to be modified.

Plug your device in or connect it, and look at dmesg.

### Bluetooth

You'll see something like this at the end (example is a first-generation PS Move controller):

```none
[541976.201331] input: Motion Controller as /devices/pci0000:00/0000:00:01.3/0000:02:00.0/usb1/1-12/1-12:1.0/bluetooth/hci0/hci0:1/0005:054C:03D5.001A/input/input62
[541976.201426] sony 0005:054C:03D5.001A: input,hidraw6: BLUETOOTH HID v0.01 Joystick [Motion Controller] on fc:01:7c:a3:cf:06
```

the HID "VID/PID" are in that long path, in the level that starts with `0005`
(`0005:054C:03D5.001A` in our example):

- 0005 means Bluetooth
- 054C:03D5 are the vendor and product ID, respectively

(They're also in the log "prefix" on the line starting with "sony" - the driver name.)

If your device also can be used over USB, check to see if these values match
between the two. If they do, you can just put `bluetooth=True` (the `usb=True`
is implied by default) in the Device entry. Otherwise, be sure to add
`usb=False` as well as a second device entry for the USB identity of the device.

### USB

You'll see something like this at the end (example is still a first-generation PS Move controller):

```none
[542674.705441] usb 3-1.2.1: USB disconnect, device number 77
[544022.781910] usb 3-1.2.1: new full-speed USB device number 78 using xhci_hcd
[544022.888540] usb 3-1.2.1: New USB device found, idVendor=054c, idProduct=03d5, bcdDevice= 2.20
[544022.888541] usb 3-1.2.1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[544022.888542] usb 3-1.2.1: Product: Motion Controller
[544022.888543] usb 3-1.2.1: Manufacturer: Sony Computer Entertainment
[544022.966925] input: Sony Computer Entertainment Motion Controller as /devices/pci0000:00/0000:00:07.1/0000:0b:00.3/usb3/3-1/3-1.2/3-1.2.1/3-1.2.1:1.0/0003:054C:03D5.001D/input/input65
[544022.967017] sony 0003:054C:03D5.001D: input,hidraw5: USB HID v1.10 Joystick [Sony Computer Entertainment Motion Controller] on usb-0000:0b:00.3-1.2.1/input0
```

Here, the VID and PID are nicely called out:

- `idVendor=054c`
- `idProduct=03d5`

Note that here, this device also can use Bluetooth, in which case it shows up
with the same VID/PID but on a different bus.

## License

Boost Software License 1.0
